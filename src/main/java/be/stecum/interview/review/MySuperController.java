package be.stecum.interview.review;

import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.servlet.http.HttpServletRequest;

public class MySuperController {

    /**SAF
     * Fetch a map with CustomHeaders
     * @param request
     * @return map with headers and value
     */
    Map<String, String> fetchHeaderMap(HttpServletRequest request) {
        Map<String, String> headerMapper = new ConcurrentHashMap<String, String>();
        Enumeration<String> names = request.getHeaderNames();

        while (names.hasMoreElements()) {
            String name = (String)names.nextElement();
            Enumeration<String> values = request.getHeaders(name);
            if (values != null) {
                while (values.hasMoreElements()) {
                    String value = (String)values.nextElement();
                    if (CustomHeader.MY_CUSTUM_ID.httpHeaderValue.equalsIgnoreCase(name)) {
                        headerMapper.put(CustomHeader.MY_CUSTUM_ID.httpHeaderValue(), value);
                    }
                    if (CustomHeader.SOME_REFERENCE.httpHeaderValue.equalsIgnoreCase(name)) {
                        headerMapper.put(CustomHeader.SOME_REFERENCE.httpHeaderValue(), value);
                    }
                    if (CustomHeader.USER_REF.httpHeaderValue.equalsIgnoreCase(name)) {
                        headerMapper.put(CustomHeader.USER_REF.httpHeaderValue(), value);
                    }
                    if (CustomHeader.EMPLOYEE.httpHeaderValue.equalsIgnoreCase(name)) {
                        headerMapper.put(CustomHeader.EMPLOYEE.httpHeaderValue(), value);
                    }
                }
            }
        }

        return headerMapper;
    }


    /*
     * Did this one first, took me 11:51 including the time to read/understand the original code (kinds were occasionally acting up).
     * Comment lines were added afterwards (not included in time
     * The spec states headers are case sensitive, if a client sends a header with a different case this should be treated as incorrect,
     * don't pollute your codebase to pander to clients sending invalid request.
     *
     */
    Map<String, String> fetchHeaderMapHowITShouldBe(HttpServletRequest request) {
        Map<String, String> headerMap = new ConcurrentHashMap<String, String>();
        for (CustomHeader customHeader : CustomHeader.values()) {
            Enumeration<String> values = request.getHeaders(customHeader.httpHeaderValue());
            if (values != null && values.hasMoreElements()){
                headerMap.put(customHeader.httpHeaderValue(), values.nextElement()); // Not entirely happy just taking the first value (return 400?) but I assume the business requirements state there can only be one value further down the stack
            }
        }
        return headerMap;
    }

    /*
     * Added this as a solution if you MUST ignore casing of the headers, took me 6:29 (but I knew the code by then)
     */
    Map<String, String> fetchHeaderIfYouMustDoItCaseinsensitive(HttpServletRequest request) {
        Map<String, String> headerMapper = new ConcurrentHashMap<String, String>();
        Enumeration<String> names = request.getHeaderNames();

        while (names.hasMoreElements()) {
            String name = (String)names.nextElement();
            CustomHeader customHeader = CustomHeader.getForHeader(name);
            if (customHeader != null){
                Enumeration<String> values = request.getHeaders(name);
                if (values != null && values.hasMoreElements()) {
                    headerMapper.put(customHeader.httpHeaderValue(), values.nextElement());
                }
            }
        }

        return headerMapper;
    }


    Map<String, String> fetchHeaderMapBetterImplementation(HttpServletRequest request) {
        throw new UnsupportedOperationException("Starting right on!!"); 
    }

    enum CustomHeader {
        MY_CUSTUM_ID("X-Cust-Id"),
        SOME_REFERENCE("X-Some-Ref"),
        USER_REF("Y-UserRef"),
        EMPLOYEE("X-EmplId");

        private final String httpHeaderValue;

        private CustomHeader(String httpHeaderValue) {
            this.httpHeaderValue = httpHeaderValue;
        }

        public String httpHeaderValue() {
            return httpHeaderValue;
        }

        public static CustomHeader getForHeader(String name) { // if performance is key I would add a static hashmap with the httpHeaderValue.toLowerCase() as a key as a fast lookup table so you can do "return map.get(name.toLowerCase());"
            for (CustomHeader customHeader : values()) {
                if (customHeader.httpHeaderValue().equalsIgnoreCase(name)){
                    return customHeader;
                }
            }
            return null;
        }
    }
}
